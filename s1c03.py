'''
From this point on, I'm not providing the number of arguments.
Part of your job is to understand the contracts of these methods
and how they fit together.
'''

def caesarEncrypt():
    pass

def caesarDecrypt():
    pass

def scoreText():
    pass

def solveS1C03():
    pass
